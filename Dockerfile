FROM php:7.2-alpine

WORKDIR /usr/src/app

RUN apk add mysql-client git zip
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN curl --silent --show-error https://getcomposer.org/installer | php
RUN php composer.phar require doctrine/migrations:1.3

COPY conf.php .
COPY db_conf.php .

CMD ["php", "vendor/bin/doctrine-migrations", "migrations:migrate",  "--configuration=conf.php", "--db-configuration=db_conf.php"]
