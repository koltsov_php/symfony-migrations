<?php

$vars = array(
    'MIGRATIONS_NAMESPACE',
    'MIGRATIONS_DIRECTORY',
);

foreach ($vars as $var) {
    $env = getenv($var);
    if (!isset($_ENV[$var]) && $env !== false) {
        $_ENV[$var] = $env;
    }
}

return [
    'migrations_namespace' => $_ENV['MIGRATIONS_NAMESPACE'],
    'migrations_directory' => $_ENV['MIGRATIONS_DIRECTORY'],
];