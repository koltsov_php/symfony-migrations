<?php

$vars = array(
    'DB_NAME',
    'DB_USER',
    'DB_PASSWORD',
    'DB_HOST',
    'DB_DRIVER',
);

foreach ($vars as $var) {
    $env = getenv($var);
    if (!isset($_ENV[$var]) && $env !== false) {
        $_ENV[$var] = $env;
    }
}

return [
    'dbname'               => $_ENV['DB_NAME'],
    'user'                 => $_ENV['DB_USER'],
    'password'             => $_ENV['DB_PASSWORD'],
    'host'                 => $_ENV['DB_HOST'],
    'driver'               => $_ENV['DB_DRIVER'],
];